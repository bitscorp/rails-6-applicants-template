# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "2.6.3"

gem "bootsnap", ">= 1.4.2", require: false
gem "pg", ">= 0.18", "< 2.0"
gem "puma", "~> 4.1"
gem "rails", "~> 6.0.3", ">= 6.0.3.1"

gem "email_validator"
gem "paranoia"
gem "ransack"

group :development, :test do
  gem "byebug", platforms: [:mri, :mingw, :x64_mingw]
  gem "database_cleaner", require: false
  gem "factory_bot_rails"
  gem "rspec-its"
  gem "rspec-json_expectations"
  gem "rspec-rails"
  gem "rubocop"
  gem "rubocop-rails"
  gem "shoulda-matchers"
  gem "simplecov", require: false
  gem "timecop"
  gem "pry"
  gem "pry-byebug"
end

group :development do
  gem "listen", "~> 3.2"
  gem "spring"
  gem "spring-watcher-listen", "~> 2.0.0"
  gem "webmock", "~> 2.3.2", require: false
end

gem "tzinfo-data", platforms: [:mingw, :mswin, :x64_mingw, :jruby]
