# frozen_string_literal: true

module Interfolio
  module ExceptionHandling

    extend ActiveSupport::Concern

    unless Rails.env.development?

      included do
        rescue_from Exception, with: :standard_error
        rescue_from ActiveRecord::RecordNotFound, with: :record_not_found_error
      end

      def standard_error(err)
        response.status = 400
        render json: { errors: [{ field: "", message: err.message }] }
      end

      def record_not_found_error(err)
        response.status = 404
        render json: { errors: [{ field: "", message: err.message.gsub(/ \[.*\]/, "") }] }
      end

    end

    def record_errors(record)
      response.status = 400
      record.errors.messages.map do |k, errors|
        { field: k, message: errors.join(", ") }
      end
    end

  end
end
