To get you started, we have already created some models, controllers and associated routes. You have complete creative control over how the app looks and functions. Please feel free to rename, move or update any existing code, or use other gems, sdks or libraries as you see fit. 

To get started, please create a branch off of master. Since this is your private project repository with Interfolio, name the branch whatever you like, our reviewers will be the only ones to see it. When you have completed the user stories below and are ready to submit the project to our reviewers, please open up a pull request against master.

Once you have completed this exercise, please answer the follow up questions below. Be sure to save your answers in a single markdown file named FOLLOW-UP.md and include it in your pull request.

### Task Requirements:

Feel free to spend as much or as little time on the exercise as you like as long as you complete the Minimum User Stories below. If time allows, feel free to complete the Preferred User Stories as well. Please be sure to include unit tests for all User Story features included. There is no need to worry about authentication or authorization with this app.

### Minimum User Stories:

```
As a user of the app
I can add an applicant's first_name, last_name, email, brief bio and date applied to a position
So that I have a central place to store applicants data
```

```
As a user of the app
I can update an applicant's first_name, last_name, email, bio or date applied
So that I can correct any misspellings or misinformation
```

```
As a user of the app
I can request a positions list of applicants the includes first_name, last_name, email and date applied
So that I can know who is applying to a specific position and how to contact them
```

```
As a user of the app
I can request a specific applicant for a position
So that I can review the applicant's first_name, last_name, email, bio and date applied
```

```
As user of the app
I can soft-delete a specific applicant for a position
So that I can keep my results clear of any applicants no longer interested
```

### Preferred User Stories (including the minimum):

```
As a user of the app
I would like to filter the positions list of applicants by first name, last name or both
So that I can filter down the result set to a specific applicant when needed
```

```
As a user of the app
I would like to sort the positions list of applicants by first name, last name or date applied
So that I can review the result set in logical groups depending on my task
```

### Submission Guidelines:

- The pull request should be against the master branch.
- The pull request should contain your project submission.
- The pull request should contain the FOLLOW-UP.md file with answers to the follow-up questions.

### Follow Up Questions:

- What libraries did you use? Why did you use them?
- If you had more time, what further improvements or new features would you add?
- Which parts are you most proud of? And why?
- Which parts did you spend the most time with? What did you find most difficult?
