### Follow Up Questions:

- What libraries did you use? Why did you use them?

```
gem "email_validator"
gem "paranoia"
gem "ransack"

group :development do
  gem "pry"
  gem "pry-byebug"
end
```

`email_validator' validates email adding `email: true` to validates method for ActiveRecord.
`paranoia` adds soft delete for ActiveRecord model.
`ransack` adds filter, sort for ActiveRecord model.
`pry`, `pry-byebug` debug tool.

- If you had more time, what further improvements or new features would you add?

Move positions controller under tenants folder because of having nested routes.

- Which parts are you most proud of? And why?

test cases are good enough to feel safe on deploy.

- Which parts did you spend the most time with? What did you find most difficult?

I don't have difficulties to write CRUD routes.
