PIPENV_BIN?=pipenv
DOCKER_COMPOSE=$(PIPENV_BIN) run docker-compose
PORT=5666

restore:
	DISABLE_DATABASE_ENVIRONMENT_CHECK=1 rake db:drop
	DISABLE_DATABASE_ENVIRONMENT_CHECK=1 rake db:create
	(psql --user admin -h 127.0.0.1 --port ${PORT} -c "CREATE ROLE postgres WITH PASSWORD ''" intf_dev || true)
	pg_restore -h 127.0.0.1 --user admin --port ${PORT} -d intf_dev $(DUMP_FILE)
.PHONY: restore

init:
	(docker network create intf-dev || true)
	pip install --user $(PIPENV_BIN)
	$(PIPENV_BIN) install
.PHONY: init

js:
	yarn install
.PHONY: js

build: init js pg
.PHONY: build

stop:
	$(DOCKER_COMPOSE) stop
.PHONY: stop

pg:
	$(DOCKER_COMPOSE) up -d
.PHONY: pg

db.create:
	createdb intf_dev --host 127.0.0.1 --port ${PORT} -U admin
	createdb intf_test --host 127.0.0.1 --port ${PORT} -U admin
.PHONY: db.create

db.drop:
	dropdb intf_dev --host 127.0.0.1 --port ${PORT} -U admin
	dropdb intf_test --host 127.0.0.1 --port ${PORT} -U admin
.PHONY: db.drop

db.seed:
	rake db:seed
.PHONY: db.drop

db.recreate: db.drop db.create migrate db.seed
.PHONY: db.recreate

install:
	rbenv exec bundle install
.PHONY: install

test:
	rbenv exec bundle exec rspec
.PHONY: test

migrate:
	RAILS_ENV=test rbenv exec bundle exec rake db:migrate
	rbenv exec bundle exec rake db:migrate
.PHONY: migrate

rollback:
	RAILS_ENV=test rbenv exec bundle exec rake db:rollback
	rbenv exec bundle exec rake db:rollback
.PHONY: rollback

console:
	rbenv exec bundle exec rails console
.PHONY: console

