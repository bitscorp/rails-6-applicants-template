# frozen_string_literal: true

Rails.application.routes.draw do
  scope defaults: { format: :json } do
    resources :positions, param: :position_id, only: [] do
      member do
        resources :applicants, controller: "positions/applicants"
      end
    end

    resources :tenants, param: :tenant_id, only: [:index, :show] do
      member do
        resources :positions, param: :position_id, only: [:index, :show]
      end
    end
  end
end
