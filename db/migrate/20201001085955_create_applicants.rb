class CreateApplicants < ActiveRecord::Migration[6.0]
  def change
    create_table :applicants do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.text :bio
      t.string :email, null: false
      t.datetime :applied_at

      t.timestamps
    end

    add_index :applicants, :email, unique: true
  end
end
