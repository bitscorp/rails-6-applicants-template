# frozen_string_literal: true

class AddPositionIdToApplicants < ActiveRecord::Migration[6.0]
  def change
    add_reference :applicants, :position, null: false, foreign_key: true
  end
end
