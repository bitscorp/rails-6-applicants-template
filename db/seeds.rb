# frozen_string_literal: true

Position.delete_all
Tenant.delete_all

# Tenants

tenants = [
  { name: "A.B.I. School of Barbering & Cosmetology" },
  { name: "Emory University" },
  { name: "Harvard University" }
]

Tenant.create(tenants)

# A.B.I. Positions

tenant = Tenant.find_by(name: "A.B.I. School of Barbering & Cosmetology")
positions = [
  { name: "Dean of Students" },
  { name: "Licensed Cosmetology Instructor" },
  { name: "Dispensary Attendan" }
]

tenant.positions.create(positions)

# Emory Positions

tenant = Tenant.find_by(name: "Emory University")
positions = [
  { name: "Dean of Students" },
  { name: "Licensed Cosmetology Instructor" },
  { name: "Dispensary Attendan" }
]

tenant.positions.create(positions)

# Harvard Positions

tenant = Tenant.find_by(name: "Harvard University")
positions = [
  { name: "Dean of Students" },
  { name: "Licensed Cosmetology Instructor" },
  { name: "Dispensary Attendan" }
]

tenant.positions.create(positions)