# frozen_string_literal: true

class Tenant < ApplicationRecord

  has_many :positions, dependent: :destroy

  validates :name, presence: true, length: { maximum: 255 }

  scope :alphabetical, -> { order(name: :asc) }

end
