# frozen_string_literal: true

class Applicant < ApplicationRecord
  acts_as_paranoid

  belongs_to :position

  validates :first_name, presence: true, length: { maximum: 255 }
  validates :last_name, presence: true, length: { maximum: 255 }
  validates :email, presence: true, length: { maximum: 255 }, email: true, uniqueness: true

  scope :for_position, ->(position_id) { where(position_id: position_id) }
end
