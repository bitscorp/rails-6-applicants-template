# frozen_string_literal: true

class Position < ApplicationRecord

  belongs_to :tenant

  validates :name, presence: true, length: { maximum: 255 }

  scope :alphabetical, -> { order(name: :asc) }
  scope :for_tenant, ->(tenant_id) { where(tenant_id: tenant_id) }

end
