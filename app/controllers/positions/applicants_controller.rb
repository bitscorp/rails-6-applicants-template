class Positions::ApplicantsController < ApplicationController
  def index
    scoped = position_applicants.ransack(params[:q])
    if scoped.sorts.empty?
      scoped.sorts = ['first_name asc', 'last_name asc']
    end

    render json: scoped.result
  end

  def show
    render json: position_applicants.find(params[:id])
  end

  def create
    attributes = applicant_attributes.merge(
      position_id: params[:position_id],
      applied_at: Time.zone.now
    )

    applicant = Applicant.new(attributes)

    if applicant.save
      render json: applicant
    else
      render json: { errors: record_errors(applicant) }
    end
  end

  def update
    applicant = position_applicants.find(params[:id])

    if applicant.update(applicant_attributes)
      render json: applicant
    else
      render json: { errors: record_errors(applicant) }
    end
  end

  def destroy
    applicant = position_applicants.find(params[:id])
    applicant.destroy!

    head(200)
  end

  private

  def applicant_attributes
    params.require(:applicant).permit!
  end

  def position_applicants
    Applicant.for_position(params[:position_id])
  end
end
