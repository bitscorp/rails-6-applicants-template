# frozen_string_literal: true

class TenantsController < ApplicationController

  def index
    render json: Tenant.alphabetical
  end

  def show
    render json: Tenant.find(params[:tenant_id])
  end

end
