# frozen_string_literal: true

require "interfolio/exception_handling"

class ApplicationController < ActionController::API

  include Interfolio::ExceptionHandling

end
