# frozen_string_literal: true

class PositionsController < ApplicationController

  def index
    render json: tenant_positions.alphabetical
  end

  def show
    render json: tenant_positions.find(params[:position_id])
  end

  private

  def tenant_positions
    Position.for_tenant(params[:tenant_id])
  end

end
