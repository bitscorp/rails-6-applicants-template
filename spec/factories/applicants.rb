FactoryBot.define do
  sequence :email do |n|
    "person#{n}@example.com"
  end

  factory :applicant do
    first_name { 'John' }
    last_name { 'Watson' }
    bio { 'Professor of Univestity' }
    email
    applied_at { Time.zone.now }

    position
  end
end
