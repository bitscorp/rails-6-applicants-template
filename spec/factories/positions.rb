# frozen_string_literal: true

FactoryBot.define do
  factory :position do
    name { "Test Position" }

    tenant
  end
end
