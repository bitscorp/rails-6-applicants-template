# frozen_string_literal: true

FactoryBot.define do
  factory :tenant do
    name { "Test University" }
  end
end
