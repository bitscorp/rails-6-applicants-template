# frozen_string_literal: true

require "rails_helper"
require "interfolio/exception_handling"

RSpec.describe Interfolio::ExceptionHandling do
  let(:klass) { ApplicationController.new { include Interfolio::ExceptionHandling } }
  let(:err) {
    out = StandardError.new(error_message)
    out.set_backtrace(["exception_handling.rb"])
    out
  }
  let(:error_message) { "message" }
  let(:fake_response) { double }
  let(:response) {
    {
      errors: [
        {
          field: "",
          message: response_message
        }
      ]
    }
  }
  let(:response_message) { "message" }

  before do
    allow(klass).to receive(:response).and_return(fake_response)
  end

  describe "#standard_error" do
    subject { klass.standard_error(err) }

    it "sets response status to 400 and renders error message" do
      expect(fake_response).to receive(:status=).with(400)
      expect(klass).to receive(:render).with(json: response)
      subject
    end
  end

  describe "#record_not_found_error" do
    subject { klass.record_not_found_error(err) }

    it "sets response status to 404 and renders error message" do
      expect(fake_response).to receive(:status=).with(404)
      expect(klass).to receive(:render).with(json: response)
      subject
    end
  end
end
