# frozen_string_literal: true

require "rails_helper"

RSpec.describe Applicant, type: :model do
  let(:position1) { FactoryBot.create(:position) }

  let(:applicant1) { FactoryBot.create(:applicant, position: position1, first_name: "John") }
  let(:applicant2) { FactoryBot.create(:applicant, position: position1, first_name: "Alex") }

  subject { FactoryBot.build(:applicant, position_id: position1.id) }

  it { is_expected.to belong_to(:position) }

  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_uniqueness_of(:email) }
  it { is_expected.to validate_length_of(:email).is_at_most(255) }

  it { is_expected.to validate_presence_of(:first_name) }
  it { is_expected.to validate_length_of(:first_name).is_at_most(255) }

  it { is_expected.to validate_presence_of(:last_name) }
  it { is_expected.to validate_length_of(:last_name).is_at_most(255) }


  describe "scopes" do
    describe ":for_position" do
      subject { described_class.for_position(position_id) }

      let(:position2) { FactoryBot.create(:position) }

      context "applicant has positions" do
        let(:position_id) { position1.id }
        it { is_expected.to eq([applicant1, applicant2]) }
      end

      context "tenant has no positions" do
        let(:position_id) { position2.id }
        it { is_expected.to eq([]) }
      end
    end
  end
end
