# frozen_string_literal: true

require "rails_helper"

RSpec.describe Position, type: :model do
  it { is_expected.to belong_to(:tenant) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_length_of(:name).is_at_most(255) }

  describe "scopes" do
    let(:tenant) { FactoryBot.create(:tenant, name: "Harvard") }
    let(:position1) { FactoryBot.create(:position, tenant: tenant, name: "Professor of Mathematics") }
    let(:position2) { FactoryBot.create(:position, tenant: tenant, name: "Dean of Students") }

    describe ":alphabetical" do
      subject { described_class.alphabetical }

      it { is_expected.to eq([position2, position1]) }
    end

    describe ":for_tenant" do
      subject { described_class.for_tenant(tenant_id) }

      let(:tenant2) { FactoryBot.create(:tenant, name: "MIT") }
      let(:tenant_id) { tenant.id }

      context "tenant has positions" do
        it { is_expected.to eq([position1, position2]) }
      end

      context "tenant has no positions" do
        let(:tenant_id) { tenant2.id }
        it { is_expected.to eq([]) }
      end
    end
  end
end
