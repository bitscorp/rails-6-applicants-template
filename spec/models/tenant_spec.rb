# frozen_string_literal: true

require "rails_helper"

RSpec.describe Tenant, type: :model do
  it { is_expected.to have_many(:positions) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_length_of(:name).is_at_most(255) }

  describe "scopes" do
    describe ":alphabetical" do
      subject { described_class.alphabetical }

      let(:tenant1) { FactoryBot.create(:tenant, name: "MIT") }
      let(:tenant2) { FactoryBot.create(:tenant, name: "Harvard") }

      it { is_expected.to eq([tenant2, tenant1]) }
    end
  end
end
