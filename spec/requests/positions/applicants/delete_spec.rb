# frozen_string_literal: true

require "rails_helper"

RSpec.describe "DELETE /positions/:position_id/applicants/:applicant_id", type: :request do
  def request(applicant_id)
    delete "/positions/#{position.id}/applicants/#{applicant_id}"
  end

  before { Timecop.freeze(now) }
  after { Timecop.return }

  let(:position) { FactoryBot.create(:position) }
  let!(:applicant) { FactoryBot.create(:applicant, position: position) }
  let(:applicant_id) { applicant.id }
  let(:now) { Time.zone.parse("2020-01-16 11:00:00") }

  context "position has applicant" do
    let(:expected_response) {
      {
        first_name: applicant.first_name,
        last_name: applicant.last_name,
        email: applicant.email,
        bio: applicant.bio,
        applied_at: "2020-01-16T11:00:00.000Z",
        created_at: "2020-01-16T11:00:00.000Z",
        updated_at: "2020-01-16T11:00:00.000Z"
      }
    }

    it "responds with a 200 on soft delete" do
      expect { request(applicant_id) }.to change { Applicant.count }.from(1).to(0)
      expect(response.status).to eq(200)

      applicants = Applicant.with_deleted
      expect(applicants.size).to eq(1)
      expect(applicants[0].id).to eq(applicant.id)
    end
  end

  context "position does not have applicant with passed id" do
    it "responds with a 404" do
      request(applicant_id + 1)
      expect(response.status).to eq(404)
    end
  end
end
