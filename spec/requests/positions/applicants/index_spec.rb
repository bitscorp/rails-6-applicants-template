# frozen_string_literal: true

require "rails_helper"

RSpec.describe "GET /positions/:position_id/applicants", type: :request do
  def request(position, filter: {}, sort: nil)
    sort = { s: sort }

    q = {}
    q.merge!(filter)
    q.merge!(sort)

    get "/positions/#{position1.id}/applicants", params: { q: q }
  end

  before { Timecop.freeze(now) }
  after { Timecop.return }

  let(:position1) { FactoryBot.create(:position) }
  let(:position2) { FactoryBot.create(:position) }
  let(:now) { Time.zone.parse("2020-01-16 11:00:00") }

  context "position has applicants" do
    let!(:applicant1) { FactoryBot.create(:applicant, position: position1, first_name: 'Alex') }
    let!(:applicant2) { FactoryBot.create(:applicant, position: position1, first_name: 'John') }
    let(:expected_response) {
      [
        {
          first_name: applicant1.first_name,
          last_name: applicant1.last_name,
          email: applicant1.email,
          bio: applicant1.bio,
          applied_at: "2020-01-16T11:00:00.000Z",
          created_at: "2020-01-16T11:00:00.000Z",
          updated_at: "2020-01-16T11:00:00.000Z"
        },
        {
          first_name: applicant2.first_name,
          last_name: applicant2.last_name,
          email: applicant2.email,
          bio: applicant2.bio,
          applied_at: "2020-01-16T11:00:00.000Z",
          created_at: "2020-01-16T11:00:00.000Z",
          updated_at: "2020-01-16T11:00:00.000Z"
        }
      ]
    }

    it "responds with a 200 and position data" do
      request(position1)
      expect(response.status).to eq(200)
      expect(response.body).to include_json(expected_response)
    end
  end

  context "position does not have applicants" do
    let(:expected_response) { [] }

    it "responds with a 200 and an empty array" do
      request(position2)
      expect(response.status).to eq(200)
      expect(response.body).to include_json(expected_response)
    end
  end

  context "filter applicants by first name, last name" do
    let!(:applicant1) { FactoryBot.create(:applicant, position: position1, first_name: 'Alex') }
    let!(:applicant2) { FactoryBot.create(:applicant, position: position1, first_name: 'John') }

    let(:expected_response) {
      [
        {
          first_name: applicant1.first_name,
          last_name: applicant1.last_name,
          email: applicant1.email,
          bio: applicant1.bio,
          applied_at: "2020-01-16T11:00:00.000Z",
          created_at: "2020-01-16T11:00:00.000Z",
          updated_at: "2020-01-16T11:00:00.000Z"
        }
      ]
    }

    it "responds with a 200 and position data" do
      request(position1, filter: { first_name: "Al" })
      expect(response.status).to eq(200)
      expect(response.body).to include_json(expected_response)
    end
  end

  context "sort applicants by first name, last name" do
    let!(:applicant1) { FactoryBot.create(:applicant, position: position1, first_name: 'Alex') }
    let!(:applicant2) { FactoryBot.create(:applicant, position: position1, first_name: 'John') }

    let(:expected_response) {
      [
        {
          first_name: applicant2.first_name,
          last_name: applicant2.last_name,
          email: applicant2.email,
          bio: applicant2.bio,
          applied_at: "2020-01-16T11:00:00.000Z",
          created_at: "2020-01-16T11:00:00.000Z",
          updated_at: "2020-01-16T11:00:00.000Z"
        },
        {
          first_name: applicant1.first_name,
          last_name: applicant1.last_name,
          email: applicant1.email,
          bio: applicant1.bio,
          applied_at: "2020-01-16T11:00:00.000Z",
          created_at: "2020-01-16T11:00:00.000Z",
          updated_at: "2020-01-16T11:00:00.000Z"
        }
      ]
    }

    it "responds with a 200 and position data" do
      request(position1, sort: 'first_name desc')
      expect(response.status).to eq(200)
      expect(response.body).to include_json(expected_response)
    end
  end
end
