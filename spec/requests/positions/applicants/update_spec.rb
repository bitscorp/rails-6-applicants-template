# frozen_string_literal: true

require "rails_helper"

RSpec.describe "POST /positions/:position_id/applicants/:applicant_id", type: :request do
  before { headers = { "CONTENT_TYPE" => "application/json" } }

  let(:now) { Time.zone.parse("2020-01-16 11:00:00") }

  before { Timecop.freeze(now) }
  after { Timecop.return }

  let(:position) { FactoryBot.create(:position) }
  let(:position_id) { position.id }
  let(:applicant) { FactoryBot.create(:applicant, position: position) }
  let(:applicant_id) { applicant.id }

  def request(attributes)
    put "/positions/#{position_id}/applicants/#{applicant_id}",
      params: { applicant: attributes }
  end

  context "valid applicant" do
    let(:expected_response) {
      {
        first_name: 'Alex',
        last_name: applicant.last_name,
        email: applicant.email,
        bio: applicant.bio,
        applied_at: "2020-01-16T11:00:00.000Z",
        created_at: "2020-01-16T11:00:00.000Z",
        updated_at: "2020-01-16T11:00:00.000Z"
      }
    }

    it "responds with a 200 and applicant data" do
      request(first_name: "Alex")
      expect(response.status).to eq(200)
      expect(response.body).to include_json(expected_response)
    end
  end

  context "invalid applicant" do
    let(:expected_response) {
      {
        errors: [
          {
            field: "email",
            message: "can't be blank, is invalid"
          }
        ]
      }
    }

    it "responds with a 400 and error data" do
      request(email: "")
      expect(response.status).to eq(400)
      expect(response.body).to include_json(expected_response)
    end
  end
end
