# frozen_string_literal: true

require "rails_helper"

RSpec.describe "GET /positions/:position_id/applicants/:applicant_id", type: :request do
  def request(applicant_id)
    get "/positions/#{position.id}/applicants/#{applicant_id}"
  end

  before { Timecop.freeze(now) }
  after { Timecop.return }

  let(:position) { FactoryBot.create(:position) }
  let(:applicant) { FactoryBot.create(:applicant, position: position) }
  let(:applicant_id) { applicant.id }
  let(:now) { Time.zone.parse("2020-01-16 11:00:00") }

  context "position has applicant" do
    let(:expected_response) {
      {
        first_name: applicant.first_name,
        last_name: applicant.last_name,
        email: applicant.email,
        bio: applicant.bio,
        applied_at: "2020-01-16T11:00:00.000Z",
        created_at: "2020-01-16T11:00:00.000Z",
        updated_at: "2020-01-16T11:00:00.000Z"
      }
    }

    it "responds with a 200 and position data" do
      request(applicant_id)
      expect(response.status).to eq(200)
      expect(response.body).to include_json(expected_response)
    end
  end

  context "position does not have applicant with passed id" do
    it "responds with a 404" do
      request(applicant_id + 1)
      expect(response.status).to eq(404)
    end
  end
end
