# frozen_string_literal: true

require "rails_helper"

RSpec.describe "POST /positions/:position_id/applicants", type: :request do
  before { headers = { "CONTENT_TYPE" => "application/json" } }

  before { Timecop.freeze(now) }
  after { Timecop.return }

  let(:position) { FactoryBot.create(:position) }
  let(:position_id) { position.id }
  let(:applicant_attributes) { FactoryBot.attributes_for(:applicant) }

  def request(attributes = applicant_attributes)
    post "/positions/#{position_id}/applicants",
      params: { applicant: attributes }
  end

  let(:now) { Time.zone.parse("2020-01-16 11:00:00") }

  context "valid applicant" do
    let(:expected_response) {
      {
        first_name: applicant_attributes[:first_name],
        last_name: applicant_attributes[:last_name],
        email: applicant_attributes[:email],
        bio: applicant_attributes[:bio],
        applied_at: "2020-01-16T11:00:00.000Z",
        created_at: "2020-01-16T11:00:00.000Z",
        updated_at: "2020-01-16T11:00:00.000Z"
      }
    }

    it "responds with a 200 and applicant data" do
      request
      expect(response.status).to eq(200)
      expect(response.body).to include_json(expected_response)
    end
  end

  context "invalid applicant" do
    let(:expected_response) {
      {
        errors: [
          {
            field: "email",
            message: "can't be blank, is invalid"
          }
        ]
      }
    }

    it "responds with a 400 and error data" do
      request(applicant_attributes.merge("email": ""))
      expect(response.status).to eq(400)
      expect(response.body).to include_json(expected_response)
    end
  end
end
