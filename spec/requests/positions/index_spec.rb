# frozen_string_literal: true

require "rails_helper"

RSpec.describe "GET /tenants/:tenant_id/positions", type: :request do
  subject do
    get "/tenants/#{tenant.id}/positions"
  end

  before { Timecop.freeze(now) }
  after { Timecop.return }

  let(:tenant) { FactoryBot.create(:tenant) }
  let(:now) { Time.zone.parse("2020-01-16 11:00:00") }

  context "tenant has positions" do
    let!(:position1) { FactoryBot.create(:position, tenant: tenant, name: "Professor of Mathematics") }
    let!(:position2) { FactoryBot.create(:position, tenant: tenant, name: "Dean of Students") }
    let(:expected_response) {
      [
        {
          id: position2.id,
          name: "Dean of Students",
          created_at: "2020-01-16T11:00:00.000Z",
          updated_at: "2020-01-16T11:00:00.000Z"
        },
        {
          id: position1.id,
          name: "Professor of Mathematics",
          created_at: "2020-01-16T11:00:00.000Z",
          updated_at: "2020-01-16T11:00:00.000Z"
        }
      ]
    }

    it "responds with a 200 and position data" do
      subject
      expect(response.status).to eq(200)
      expect(response.body).to include_json(expected_response)
    end
  end

  context "tenant does not have positions" do
    let(:expected_response) { [] }

    it "responds with a 200 and an empty array" do
      subject
      expect(response.status).to eq(200)
      expect(response.body).to include_json(expected_response)
    end
  end
end
