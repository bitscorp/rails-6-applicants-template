# frozen_string_literal: true

require "rails_helper"

RSpec.describe "GET /tenants/:tenant_id/positions/:position_id", type: :request do
  subject do
    get "/tenants/#{tenant_id}/positions/#{position_id}"
  end

  before { Timecop.freeze(now) }
  after { Timecop.return }

  let(:tenant1) { FactoryBot.create(:tenant) }
  let!(:position1) { FactoryBot.create(:position, tenant: tenant1, name: "First Test Position") }
  let(:tenant2) { FactoryBot.create(:tenant) }
  let!(:position2) { FactoryBot.create(:position, tenant: tenant2, name: "Another Test Position") }
  let(:tenant_id) { tenant1.id }
  let(:position_id) { position1.id }
  let(:now) { Time.zone.parse("2020-01-16 11:00:00") }

  context "valid position that belongs to tenant" do
    let(:expected_response) {
      {
        id: position1.id,
        name: "First Test Position",
        created_at: "2020-01-16T11:00:00.000Z",
        updated_at: "2020-01-16T11:00:00.000Z"
      }
    }

    it "responds with a 200 and tenant data" do
      subject
      expect(response.status).to eq(200)
      expect(response.body).to include_json(expected_response)
    end
  end

  context "valid position that belongs to another tenant" do
    let(:tenant_id) { tenant1.id }
    let(:position_id) { position2.id }
    let(:expected_response) {
      {
        errors: [
          {
            field: "",
            message: "Couldn't find Position with 'id'=#{position2.id}"
          }
        ]
      }
    }

    it "responds with a 404 and error data" do
      subject
      expect(response.status).to eq(404)
      expect(response.body).to include_json(expected_response)
    end
  end

  context "invalid position for valid tenant" do
    let(:tenant_id) { tenant1.id }
    let(:position_id) { 0 }
    let(:expected_response) {
      {
        errors: [
          {
            field: "",
            message: "Couldn't find Position with 'id'=0"
          }
        ]
      }
    }

    it "responds with a 404 and error data" do
      subject
      expect(response.status).to eq(404)
      expect(response.body).to include_json(expected_response)
    end
  end

  context "valid position for invalid tenant" do
    let(:tenant_id) { 0 }
    let(:position_id) { position1.id }
    let(:expected_response) {
      {
        errors: [
          {
            field: "",
            message: "Couldn't find Position with 'id'=#{position_id}"
          }
        ]
      }
    }

    it "responds with a 404 and error data" do
      subject
      expect(response.status).to eq(404)
      expect(response.body).to include_json(expected_response)
    end
  end
end
