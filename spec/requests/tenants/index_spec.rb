# frozen_string_literal: true

require "rails_helper"

RSpec.describe "GET /tenants", type: :request do
  subject do
    get "/tenants"
  end

  before { Timecop.freeze(now) }
  after { Timecop.return }

  let(:now) { Time.zone.parse("2020-01-16 11:00:00") }

  context "tenants exist" do
    let!(:tenant1) { FactoryBot.create(:tenant, name: "Missouri State") }
    let!(:tenant2) { FactoryBot.create(:tenant, name: "Harvard") }
    let(:expected_response) {
      [
        {
          id: tenant2.id,
          name: "Harvard",
          created_at: "2020-01-16T11:00:00.000Z",
          updated_at: "2020-01-16T11:00:00.000Z"
        },
        {
          id: tenant1.id,
          name: "Missouri State",
          created_at: "2020-01-16T11:00:00.000Z",
          updated_at: "2020-01-16T11:00:00.000Z"
        }
      ]
    }

    it "responds with a 200 and tenant data" do
      subject
      expect(response.status).to eq(200)
      expect(response.body).to include_json(expected_response)
    end
  end

  context "no tenants exist" do
    let(:expected_response) { [] }

    it "responds with a 200 and an empty array" do
      subject
      expect(response.status).to eq(200)
      expect(response.body).to include_json(expected_response)
    end
  end
end
