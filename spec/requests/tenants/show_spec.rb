# frozen_string_literal: true

require "rails_helper"

RSpec.describe "GET /tenants/:tenant_id", type: :request do
  subject do
    get "/tenants/#{tenant_id}"
  end

  before { Timecop.freeze(now) }
  after { Timecop.return }

  let(:tenant) { FactoryBot.create(:tenant) }
  let(:tenant_id) { tenant.id }
  let(:now) { Time.zone.parse("2020-01-16 11:00:00") }

  context "valid tenant" do
    let(:expected_response) {
      {
        id: tenant.id,
        name: "Test University",
        created_at: "2020-01-16T11:00:00.000Z",
        updated_at: "2020-01-16T11:00:00.000Z"
      }
    }

    it "responds with a 200 and tenant data" do
      subject
      expect(response.status).to eq(200)
      expect(response.body).to include_json(expected_response)
    end
  end

  context "invalid tenant" do
    let(:tenant_id) { 0 }
    let(:expected_response) {
      {
        errors: [
          {
            field: "",
            message: "Couldn't find Tenant with 'id'=0"
          }
        ]
      }
    }

    it "responds with a 404 and error data" do
      subject
      expect(response.status).to eq(404)
      expect(response.body).to include_json(expected_response)
    end
  end
end
